#!/usr/bin/env python
# coding: utf-8

# In[1]:


# This script updates Tail Stacker's anchor lines 
# when user changes Tail Stacker or anchor points' locations manually.
# Author: Mehdi Pira - Esri Australia
# Created: 20200106

from arcgis.gis import GIS
from arcgis.features import SpatialDataFrame
from arcgis.features import FeatureLayer
import pandas as pd
import numpy as np
import time
import datetime
import config_TS
import os
from arcgis import geometry
from copy import deepcopy

# define variables
gis = None
# df = None
# append_item = None
tsGPS_item = None
# concentrator_gpsdata = None
# monthsToKeep = str(datetime.datetime.today() - datetime.timedelta(config_TS.CONFIG['NoOfMonths']*365/12))
# whereQuery = "Date_Stamp < " + "\'" + monthsToKeep + "\'" + ""

def connect():
    # connnect to ArcGIS Online
    try:
        global gis
        gis = GIS(config_TS.CONFIG['url'], config_TS.CONFIG['username'], config_TS.CONFIG['password'], verify_cert=True)
    except:
        raise

# def readCSV():
#     # read the csv file and create column names and also get rid of empty rows
#     try:
#         global df
#         inputCSV = os.path.join(config_TS.CONFIG['dataWorkspace'], config_TS.CONFIG['csvfile'])
#         df = pd.read_csv(inputCSV, header=None)
#         df = df.dropna(how='all', axis=1)
#         df.columns =['Concentrator', 'Format', 'Time', 'Valid', 'Latitude', 'S_N', 'Longitude', 'E_W', 'Pond_RL', 'X1', 'X2', 'Date_Stamp']
#         df.dropna(axis=0, how='any', thresh=None, subset=None, inplace=True)
#     except:
#         raise
    

# def featureEngineer():
#     try:
#         # Date and Time manipulation
#         df['Time']=df['Time'].apply(lambda x: '{0:0>6}'.format(x))
#         df['Time'] = df['Time'].apply(str)  
#         df['Date_Stamp'] = df['Date_Stamp'].astype(float).astype(int)
#         dtime = df['Date_Stamp'].apply(str) + ' ' + df['Time']
#         df['Date_Stamp'] = pd.to_datetime(dtime, format='%d%m%y %H%M%S', utc=True)

#         # Flagging the latest recorded time of the gps location as active
#         df['Loc_Status'] = np.where(df['Date_Stamp'] >= df['Date_Stamp'].max(), 'active', 'inactive')
#     except:
#         raise

# def savePublish():
#     # publish the csv into ArcGIS Online
#     try:
#         global append_item
#         global outTempcsv
#         outTempcsv = os.path.join(config_TS.CONFIG['tempWorkspace'], config_TS.CONFIG['tempCSVname'])
#         df.to_csv(outTempcsv)        
#     except:
#         raise

# def updateStatus():
#     try:
#         # Update status field
#         global concentrator_gpsdata
#         global concentratorGPS_item
#         concentratorGPS_item = gis.content.search('type: "Feature Service" AND title: "GPS Concentrator"')
#         concentrator_gpsdata = concentratorGPS_item[0].layers[0]
#         concentrator_activeRow = concentrator_gpsdata.query(where="Loc_Status = 'active'")
#         if concentrator_activeRow:
#             concentrator_activeRow.features[0].attributes['Loc_Status'] = 'inactive'
#             concentrator_gpsdata.edit_features(updates = concentrator_activeRow.features)
#         else:
#             print('There is no active row!')
#     except Exception as e:
#         raise
        
# def appendingData():
#     # append the temp feature service which is the latest gps data captured to the existing CJ1 gps feature service
#     try:
#         concentrator_fset = concentrator_gpsdata.query()
#         new_rows = pd.read_csv(outTempcsv)
        
#         features_to_be_added = []

#         # get a template feature object
#         template_feature = deepcopy(concentrator_fset.features[0])

#         # loop through each row and add to the list of features to be added
#         for row in new_rows.iterrows():
#             new_feature = deepcopy(template_feature)

#             #print
#             print("Appending to " + row[1]['Concentrator'])

#             #get geometries in the destination coordinate system
#             input_geometry = {'y':float(row[1]['Latitude']),
#                                'x':float(row[1]['Longitude'])}
#             output_geometry = geometry.project(geometries = [input_geometry],
#                                                in_sr = 4326, 
#                                                out_sr = concentrator_fset.spatial_reference['latestWkid'],
#                                                gis = gis)

#             # assign the updated values
#             new_feature.geometry = output_geometry[0]
#             new_feature.attributes['Longitude'] = float(row[1]['Longitude'])
#             new_feature.attributes['Concentrator'] = row[1]['Concentrator']
#             new_feature.attributes['Format'] = row[1]['Format']
#             new_feature.attributes['Time'] = row[1]['Time']
#             new_feature.attributes['Latitude'] = float(row[1]['Latitude'])
#             new_feature.attributes['Valid'] = row[1]['Valid']
#             new_feature.attributes['S_N'] = row[1]['S_N']
#             new_feature.attributes['E_W'] = row[1]['E_W']
#             new_feature.attributes['Pond_RL'] = float(row[1]['Pond_RL'])
#             new_feature.attributes['X1'] = int(row[1]['X1'])
#             new_feature.attributes['X2'] = int(row[1]['X2'])
#             new_feature.attributes['Date_Stamp'] = row[1]['Date_Stamp']
#             new_feature.attributes['Loc_Status'] = row[1]['Loc_Status']

#             #add this to the list of features to be updated
#             features_to_be_added.append(new_feature)
        
#         concentrator_gpsdata.edit_features(adds = features_to_be_added)
        
#     except:
#         raise

def updateAnchorLine():
    # update anchor lines by feeding the active dredge and anchor coordinates
    try:
        tsGPS_item = gis.content.search('type: "Feature Service" AND title: "Tail Stacker T"')
        tsGPS = tsGPS_item[0].layers[0]
        ts_xy = []
        Longitude = tsGPS.query(where="Tail_Stacker_Status = 'In Use'").features[0].geometry['x']
        Latitude = tsGPS.query(where="Tail_Stacker_Status = 'In Use'").features[0].geometry['y']
        ts_xy.append(Longitude)
        ts_xy.append(Latitude)

        # Getting anchor points coordinates
        anchor_points = gis.content.search('type:"Feature Service" AND title: "Anchor points"')
        anchor_layer = anchor_points[0].layers[0]
        ts_anchor_lyr = anchor_layer.query(where = "Name = 'PORT_AFT_TS' OR Name = 'PORT_FWD_TS' OR Name = 'STB_AFT_TS' OR Name = 'STB_FWD_TS'")
        ts_anchor_x = [coords.geometry['x'] for coords in ts_anchor_lyr]
        ts_anchor_y = [coords.geometry['y'] for coords in ts_anchor_lyr]
        ts_anchor_xy = [list(xy) for xy in zip(ts_anchor_x, ts_anchor_y)]
        
        # Getting anchor lines
        ts_anchor_lines = gis.content.search('type:"Feature Service" AND title: "Tail Stacker Anchor Lines"')
        ts_anchor_layer = ts_anchor_lines[0].layers[0].query()
        
        ts_updateFeature = [{"geometry": {"paths": [[ts_xy, ts_anchor_xy[0]]]}, "attributes": {"OBJECTID": 7, "Status_TSAL": "In Use", "Shape__Length": 0.00455297677538328, "GlobalID": "dfc54136-f327-41da-bd92-a438bccebb4a", "CreationDate": 1568850786813, "Creator": "leonard.mills_Tronox", "EditDate": 1577948431831, "Editor": "murugan.subramanian_Tronox"}},
                            {"geometry": {"paths": [[ts_xy, ts_anchor_xy[1]]]}, "attributes": {"OBJECTID": 8, "Status_TSAL": "In Use", "Shape__Length": 0.00370894496987844, "GlobalID": "e658595d-a6f1-43e8-a89c-8cf5d6c9d30d", "CreationDate": 1568850793479, "Creator": "leonard.mills_Tronox", "EditDate": 1573606317794, "Editor": "leonard.mills_Tronox"}},
                            {"geometry": {"paths": [[ts_xy, ts_anchor_xy[2]]]}, "attributes": {"OBJECTID": 9, "Status_TSAL": "In Use", "Shape__Length": 0.00373151577542218, "GlobalID": "ce60dfa1-3c69-4717-a1ac-bbcd5c3c80b5", "CreationDate": 1568850802961, "Creator": "leonard.mills_Tronox", "EditDate": 1575962061173, "Editor": "robert.Snijders_Tronox"}},
                            {"geometry": {"paths": [[ts_xy, ts_anchor_xy[3]]]}, "attributes": {"OBJECTID": 11, "Status_TSAL": "In Use", "Shape__Length": 0.00422131628330389, "GlobalID": "3c789a43-177d-40bf-ad12-d85aa7e3798b", "CreationDate": 1575962051212, "Creator": "robert.Snijders_Tronox", "EditDate": 1577948442655, "Editor": "murugan.subramanian_Tronox"}}]

               
        ts_anchor_lyr = ts_anchor_lines[0].layers[0]
        ts_anchor_lyr.edit_features(updates = ts_updateFeature)
        
    except:
        raise

# def timeConvert(monthsToDelete):
#     try:
#         d = datetime.datetime(monthsToKeep.year, monthsToKeep.month, monthsToKeep.day, monthsToKeep.hour, monthsToKeep.minute, monthsToKeep.second)
#         unixtime = time.mktime(d.timetuple())
#         unixOutput = int(unixtime*1000)
#         return unixOutput
#     except:
#         raise
        
# def deleteFeatures():
#     # selects features based on months defined and deletes them
#     try:
#         concentratorGPS_item = gis.content.search('type: "Feature Service" AND title: "GPS Concentrator"')
#         concentrator_gpsdata = concentratorGPS_item[0].layers[0]
#         concentrator_gpsdataQ = concentratorGPS_item[0].layers[0].query(where = whereQuery)
#         deleteFeatures = concentrator_gpsdataQ.features

#         object_ids = [str(f.get_value('ObjectId')) for f in deleteFeatures]
#         object_ids_str = ",".join(object_ids)
#         if object_ids:
#             concentrator_gpsdata.edit_features(deletes = object_ids_str)
#         else:
#             print ("There are no rows to delete!")
#     except:
#         pass

def init():
    connect()
#     readCSV()
#     featureEngineer()
#     savePublish()
#     updateStatus()
#     appendingData()
    updateAnchorLine()
#     deleteFeatures()
    
def main():
    init()

if __name__ == '__main__':
    main()


# In[ ]:




