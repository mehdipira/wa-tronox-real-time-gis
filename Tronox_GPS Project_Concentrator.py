#!/usr/bin/env python
# coding: utf-8

# In[2]:


# This script reads gps data as csv and updates Concentrator (Wet Plant) feature service
# and the anchor lines attached to it in real time.
# Author: Mehdi Pira - Esri Australia
# Created: 20191223

from arcgis.gis import GIS
from arcgis.features import SpatialDataFrame
from arcgis.features import FeatureLayer
import pandas as pd
import numpy as np
import time
import datetime
import config_concentrator
import os
from arcgis import geometry
from copy import deepcopy

# define variables
gis = None
df = None
append_item = None
concentratorGPS_item = None
concentrator_gpsdata = None
monthsToKeep = str(datetime.datetime.today() - datetime.timedelta(config_concentrator.CONFIG['NoOfMonths']*365/12))
whereQuery = "Date_Stamp < " + "\'" + monthsToKeep + "\'" + ""

def connect():
    # connnect to ArcGIS Online
    try:
        global gis
        gis = GIS(config_concentrator.CONFIG['url'], config_concentrator.CONFIG['username'], config_concentrator.CONFIG['password'], verify_cert=True)
    except:
        raise

def readCSV():
    # read the csv file and create column names and also get rid of empty rows
    try:
        global df
        inputCSV = os.path.join(config_concentrator.CONFIG['dataWorkspace'], config_concentrator.CONFIG['csvfile'])
        df = pd.read_csv(inputCSV, header=None)
        df = df.dropna(how='all', axis=1)
        df.columns =['Concentrator', 'Format', 'Time', 'Valid', 'Latitude', 'S_N', 'Longitude', 'E_W', 'Pond_RL', 'X1', 'X2', 'Date_Stamp']
        df.dropna(axis=0, how='any', thresh=None, subset=None, inplace=True)
    except:
        raise
    

def featureEngineer():
    try:
        # Date and Time manipulation
        df['Time']=df['Time'].apply(lambda x: '{0:0>6}'.format(x))
        df['Time'] = df['Time'].apply(str)  
        df['Date_Stamp'] = df['Date_Stamp'].astype(float).astype(int)
        dtime = df['Date_Stamp'].apply(str) + ' ' + df['Time']
        df['Date_Stamp'] = pd.to_datetime(dtime, format='%d%m%y %H%M%S', utc=True)

        # Flagging the latest recorded time of the gps location as active
        df['Loc_Status'] = np.where(df['Date_Stamp'] >= df['Date_Stamp'].max(), 'active', 'inactive')
    except:
        raise

def savePublish():
    # publish the csv into ArcGIS Online
    try:
        global append_item
        global outTempcsv
        outTempcsv = os.path.join(config_concentrator.CONFIG['tempWorkspace'], config_concentrator.CONFIG['tempCSVname'])
        df.to_csv(outTempcsv)        
    except:
        raise

def updateStatus():
    try:
        # Update status field
        global concentrator_gpsdata
        global concentratorGPS_item
        concentratorGPS_item = gis.content.search('type: "Feature Service" AND title: "GPS Concentrator"')
        concentrator_gpsdata = concentratorGPS_item[0].layers[0]
        concentrator_activeRow = concentrator_gpsdata.query(where="Loc_Status = 'active'")
        if concentrator_activeRow:
            concentrator_activeRow.features[0].attributes['Loc_Status'] = 'inactive'
            concentrator_gpsdata.edit_features(updates = concentrator_activeRow.features)
        else:
            print('There is no active row!')
    except Exception as e:
        raise
        
def appendingData():
    # append the temp feature service which is the latest gps data captured to the existing CJ1 gps feature service
    try:
        concentrator_fset = concentrator_gpsdata.query()
        new_rows = pd.read_csv(outTempcsv)
        
        features_to_be_added = []

        # get a template feature object
        template_feature = deepcopy(concentrator_fset.features[0])

        # loop through each row and add to the list of features to be added
        for row in new_rows.iterrows():
            new_feature = deepcopy(template_feature)

            #print
            print("Appending to " + row[1]['Concentrator'])

            #get geometries in the destination coordinate system
            input_geometry = {'y':float(row[1]['Latitude']),
                               'x':float(row[1]['Longitude'])}
            output_geometry = geometry.project(geometries = [input_geometry],
                                               in_sr = 4326, 
                                               out_sr = concentrator_fset.spatial_reference['latestWkid'],
                                               gis = gis)

            # assign the updated values
            new_feature.geometry = output_geometry[0]
            new_feature.attributes['Longitude'] = float(row[1]['Longitude'])
            new_feature.attributes['Concentrator'] = row[1]['Concentrator']
            new_feature.attributes['Format'] = row[1]['Format']
            new_feature.attributes['Time'] = row[1]['Time']
            new_feature.attributes['Latitude'] = float(row[1]['Latitude'])
            new_feature.attributes['Valid'] = row[1]['Valid']
            new_feature.attributes['S_N'] = row[1]['S_N']
            new_feature.attributes['E_W'] = row[1]['E_W']
            new_feature.attributes['Pond_RL'] = float(row[1]['Pond_RL'])
            new_feature.attributes['X1'] = int(row[1]['X1'])
            new_feature.attributes['X2'] = int(row[1]['X2'])
            new_feature.attributes['Date_Stamp'] = row[1]['Date_Stamp']
            new_feature.attributes['Loc_Status'] = row[1]['Loc_Status']

            #add this to the list of features to be updated
            features_to_be_added.append(new_feature)
        
        concentrator_gpsdata.edit_features(adds = features_to_be_added)
        
    except:
        raise

def updateAnchorLine():
    # update anchor lines by feeding the active dredge and anchor coordinates
    try:
        activePoint = concentrator_gpsdata.query(where="Loc_Status = 'active'")
        concentrator_x = [x for x in activePoint.sdf.Longitude.values]
        concentrator_y = [y for y in activePoint.sdf.Latitude.values]
        concentrator_xy = [list(xy) for xy in zip(concentrator_x, concentrator_y)]

        anchor_points = gis.content.search('type:"Feature Service" AND title: "Anchor points"')
        anchor_layer = anchor_points[0].layers[0]
        wetPlant_anchor_lyr = anchor_layer.query(where = "Name = 'PORT_MID_WP' OR Name = 'PORT_FWD_WP' OR Name = 'PORT_AFT_WP' OR Name = 'STB_MID_WP' OR Name = 'STB_FWD_WP' OR Name = 'STB_AFT_WP'")
        wetPlant_anchor_x = [coords.geometry['x'] for coords in wetPlant_anchor_lyr]
        wetPlant_anchor_y = [coords.geometry['y'] for coords in wetPlant_anchor_lyr]
        wetPlant_anchor_xy = [list(xy) for xy in zip(wetPlant_anchor_x, wetPlant_anchor_y)]

        wetPlant_anchor_lines = gis.content.search('type:"Feature Service" AND title: "Wet Plant Anchor Lines"')
        #wetPlant_anchor_layer = wetPlant_anchor_lines[0].layers[0].query()

        wetPlant_updateFeature = [{"geometry": {"paths": [[concentrator_xy[0], wetPlant_anchor_xy[0]]]}, "attributes": {"OBJECTID": 13, "Status_WTAL": "In Use", "created_user": "mat.helms_Tronox", "created_date": 1568842777272, "last_edited_user": "robert.Snijders_Tronox", "last_edited_date": 1575962080357, "Shape__Length": 0.00516857946569844, "GlobalID": "13370681-2626-4f36-b0fa-93fd5df70659"}},
                                 {"geometry": {"paths": [[concentrator_xy[0], wetPlant_anchor_xy[1]]]}, "attributes": {"OBJECTID": 14, "Status_WTAL": "In Use", "created_user": "mat.helms_Tronox", "created_date": 1568842824709, "last_edited_user": "robert.Snijders_Tronox", "last_edited_date": 1575515193220, "Shape__Length": 0.0063168292859334, "GlobalID": "8bdcdeeb-be2e-48f5-8fdc-59f1c16f6a61"}},
                                 {"geometry": {"paths": [[concentrator_xy[0], wetPlant_anchor_xy[2]]]}, "attributes": {"OBJECTID": 15, "Status_WTAL": "In Use", "created_user": "mat.helms_Tronox", "created_date": 1568842872312, "last_edited_user": "robert.Snijders_Tronox", "last_edited_date": 1575515164369, "Shape__Length": 0.00404099731502961, "GlobalID": "1b74b72a-dbb7-4cf4-ba76-76d209a84170"}},
                                 {"geometry": {"paths": [[concentrator_xy[0], wetPlant_anchor_xy[3]]]}, "attributes": {"OBJECTID": 17, "Status_WTAL": "In Use", "created_user": "leonard.mills_Tronox", "created_date": 1568850642705, "last_edited_user": "robert.Snijders_Tronox", "last_edited_date": 1577065672886, "Shape__Length": 0.00210103087701619, "GlobalID": "07614516-f857-4ec1-804b-d651d9b0e320"}},
                                 {"geometry": {"paths": [[concentrator_xy[0], wetPlant_anchor_xy[4]]]}, "attributes": {"OBJECTID": 18, "Status_WTAL": "In Use", "created_user": "leonard.mills_Tronox", "created_date": 1568850651766, "last_edited_user": "robert.Snijders_Tronox", "last_edited_date": 1575515181311, "Shape__Length": 0.0028681118349102, "GlobalID": "fd5798ab-c594-4e62-ad5a-09d2c1455361"}},
                                 {"geometry": {"paths": [[concentrator_xy[0], wetPlant_anchor_xy[5]]]}, "attributes": {"OBJECTID": 19, "Status_WTAL": "In Use", "created_user": "mat.helms_Tronox", "created_date": 1568854480921, "last_edited_user": "robert.Snijders_Tronox", "last_edited_date": 1577065679290, "Shape__Length": 0.00165428768389742, "GlobalID": "36bc9694-8832-4f48-b010-782d41588c12"}}]
        
        concentrator_anchor_lyr = wetPlant_anchor_lines[0].layers[0]
        concentrator_anchor_lyr.edit_features(updates = wetPlant_updateFeature)
    except:
        raise

# def timeConvert(monthsToDelete):
#     try:
#         d = datetime.datetime(monthsToKeep.year, monthsToKeep.month, monthsToKeep.day, monthsToKeep.hour, monthsToKeep.minute, monthsToKeep.second)
#         unixtime = time.mktime(d.timetuple())
#         unixOutput = int(unixtime*1000)
#         return unixOutput
#     except:
#         raise
        
def deleteFeatures():
    # selects features based on months defined and deletes them
    try:
        concentratorGPS_item = gis.content.search('type: "Feature Service" AND title: "GPS Concentrator"')
        concentrator_gpsdata = concentratorGPS_item[0].layers[0]
        concentrator_gpsdataQ = concentratorGPS_item[0].layers[0].query(where = whereQuery)
        deleteFeatures = concentrator_gpsdataQ.features

        object_ids = [str(f.get_value('ObjectId')) for f in deleteFeatures]
        object_ids_str = ",".join(object_ids)
        if object_ids:
            concentrator_gpsdata.edit_features(deletes = object_ids_str)
        else:
            print ("There are no rows to delete!")
    except:
        pass

def init():
    connect()
    readCSV()
    featureEngineer()
    savePublish()
    updateStatus()
    appendingData()
    updateAnchorLine()
    deleteFeatures()
    
def main():
    init()

if __name__ == '__main__':
    main()


# In[ ]:




