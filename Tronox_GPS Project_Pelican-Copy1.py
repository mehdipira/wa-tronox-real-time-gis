
# coding: utf-8

# In[2]:


# This script reads gps data as csv and updates Pelican dredge feature service
# and the anchor lines attached to it in real time.
# Author: Mehdi Pira - Esri Australia
# Created: 20190321
# Updated: 20190701

from arcgis.gis import GIS
from arcgis.features import SpatialDataFrame
from arcgis.features import FeatureLayer
import pandas as pd
import numpy as np
import time
import datetime
import config_pelican
import os
from arcgis import geometry
from copy import deepcopy

# define variables
gis = None
df = None
append_item = None
PELICANGPS_item = None
pelican_gpsdata = None
monthsToKeep = datetime.datetime.today() - datetime.timedelta(config_pelican.CONFIG['NoOfMonths']*365/12)

def connect():
    # connnect to ArcGIS Online
    try:
        global gis
        gis = GIS(config_pelican.CONFIG['url'], config_pelican.CONFIG['username'], config_pelican.CONFIG['password'], verify_cert=True)
    except:
        raise

def readCSV():
    # read the csv file and create column names and also get rid of empty rows
    try:
        global df
        inputCSV = os.path.join(config_pelican.CONFIG['dataWorkspace'], config_pelican.CONFIG['csvfile'])
        df = pd.read_csv(inputCSV, header=None)
        df.columns =['Dredge', 'Format', 'Time', 'Valid', 'Latitude', 'S_N', 'Longitude', 'E_W', 'Speed', 'True_Course', 'Date_Stamp', 'Variation', 'W_E', 'Check_Sum']
        df.dropna(axis=0, how='any', thresh=None, subset=None, inplace=True)
    except:
        raise

def featureEngineer():
    # convert latitudes/longitudes from DMD to DD, standardize date and time columns and flag latest date as active
    try:
        # Latitude conversion
        df['Latitude'] = df['Latitude'].astype(str)
        degree = df['Latitude'].str.split('.').str[0].str[:2].astype(float)
        minutes = (df['Latitude'].str.split('.').str[0].str[2:])
        decimalMinute = (minutes + '.' + df['Latitude'].str.split('.').str[1]).astype(float)/60
        df['Latitude'] = degree + decimalMinute
        df['Latitude'] = df['Latitude'] * -1

        # Longitude conversion
        df['Longitude'] = df['Longitude'].astype(str)
        degree2 = df['Longitude'].str.split('.').str[0].str[:3].astype(float)
        minutes2 = df['Longitude'].str.split('.').str[0].str[3:]
        decimalMinute2 = (minutes2 + '.' + df['Longitude'].str.split('.').str[1]).astype(float)/60
        df['Longitude'] = degree2 + decimalMinute2

        # Date and Time manipulation
        df['Time']=df['Time'].apply(lambda x: '{0:0>8}'.format(x))
        df['Time'] = df['Time'].apply(str)  
        df['Date_Stamp'] = df['Date_Stamp'].astype(float).astype(int)
        dtime = df['Date_Stamp'].apply(str) + ' ' + df['Time']
        df['Date_Stamp'] = pd.to_datetime(dtime, format='%d%m%y %H%M%S.%f', utc=True)

        # Flagging the latest recorded time of the gps location as active
        df['Loc_Status'] = np.where(df['Date_Stamp'] >= df['Date_Stamp'].max(), 'active', 'inactive')
    except:
        raise

def savePublish():
    # publish the csv into ArcGIS Online
    try:
        global append_item
        global outTempcsv
        outTempcsv = os.path.join(config_pelican.CONFIG['tempWorkspace'], config_pelican.CONFIG['tempCSVname'])
        df.to_csv(outTempcsv)
    except:
        raise

def updateStatus():
    try:
        # Updating status field
        global pelican_gpsdata
        global PELICANGPS_item
        PELICANGPS_item = gis.content.search('type: "Feature Service" AND title: "GPS Pelican"')
        pelican_gpsdata = PELICANGPS_item[0].layers[0]
        pelican_activeRow = pelican_gpsdata.query(where="Loc_Status = 'active'")
        if pelican_activeRow:
            pelican_activeRow.features[0].attributes['Loc_Status'] = 'inactive'
            pelican_gpsdata.edit_features(updates = pelican_activeRow.features)
        else:
            print('There is no active row!')
    except Exception as e:
        raise
        
def appendingData():
    # append the temp feature service which is the latest gps data captured to the existing Pelican gps feature service
    try:
        pelican_fset = pelican_gpsdata.query()
        new_rows = pd.read_csv(outTempcsv)
        
        features_to_be_added = []

        # get a template feature object
        template_feature = deepcopy(pelican_fset.features[0])

        # loop through each row and add to the list of features to be added
        for row in new_rows.iterrows():
            new_feature = deepcopy(template_feature)

            #print
            print("Appending to " + row[1]['Dredge'])

            #get geometries in the destination coordinate system
            input_geometry = {'y':float(row[1]['Latitude']),
                               'x':float(row[1]['Longitude'])}
            output_geometry = geometry.project(geometries = [input_geometry],
                                               in_sr = 4326, 
                                               out_sr = pelican_fset.spatial_reference['latestWkid'],
                                               gis = gis)

            # assign the updated values
            new_feature.geometry = output_geometry[0]
            new_feature.attributes['Longitude'] = float(row[1]['Longitude'])
            new_feature.attributes['Dredge'] = row[1]['Dredge']
            new_feature.attributes['Format'] = row[1]['Format']
            new_feature.attributes['Time'] = float(row[1]['Time'])
            new_feature.attributes['Latitude'] = float(row[1]['Latitude'])
            new_feature.attributes['Valid'] = row[1]['Valid']
            new_feature.attributes['S_N'] = row[1]['S_N']
            new_feature.attributes['E_W'] = row[1]['E_W']
            new_feature.attributes['Speed'] = float(row[1]['Speed'])
            new_feature.attributes['True_Course'] = int(row[1]['True_Course'])
            new_feature.attributes['Date_Stamp'] = row[1]['Date_Stamp']
            new_feature.attributes['Variation'] = float(row[1]['Variation'])
            new_feature.attributes['W_E'] = row[1]['W_E']
            new_feature.attributes['Check_Sum'] = row[1]['Check_Sum']
            new_feature.attributes['Loc_Status'] = row[1]['Loc_Status']

            #add this to the list of features to be updated
            features_to_be_added.append(new_feature)
        
        pelican_gpsdata.edit_features(adds = features_to_be_added)
        
    except:
        raise

def updateAnchorLine():
    # update anchor lines by feeding the active dredge and anchor coordinates
    try:
        activePointP = pelican_gpsdata.query(where="Loc_Status = 'active'")
        pelican_dredge_x = [x for x in activePointP.df.Longitude.values]
        pelican_dredge_y = [y for y in activePointP.df.Latitude.values]
        pelican_dredge_xy = [list(xy) for xy in zip(pelican_dredge_x, pelican_dredge_y)]

        anchor_points = gis.content.search('type:"Feature Service" AND title: "Anchor points"')
        anchor_layer = anchor_points[0].layers[0]
        pelican_anchor_lyr = anchor_layer.query(where = "(Name = 'Pelican port anchor' OR Name = 'Pelican starboard anchor') OR Status = 'active'")
        pelican_anchor_x = [coords.geometry['x'] for coords in pelican_anchor_lyr]
        pelican_anchor_y = [coords.geometry['y'] for coords in pelican_anchor_lyr]
        pelican_anchor_xy = [list(xy) for xy in zip(pelican_anchor_x, pelican_anchor_y)]

        pelican_anchor_lines = gis.content.search('type:"Feature Service" AND title: "Pelican Anchor Lines"')
        #cj1_anchor_layer = cj1_anchor_lines[0].layers[0].query()

        pelican_updateFeature = [{"geometry": {"paths": [[pelican_dredge_xy[0], pelican_anchor_xy[0]]]}, "attributes": {"OBJECTID": 1, "Pelican_anchorLine_status": "active", "Shape__Length": 92.0114307354861, "GlobalID": "e4ffd6b9-566f-47bc-9cba-b4cf32a70171", "CreationDate": 1552446220252, "Creator": "robert.Snijders_Tronox", "EditDate": 1552446220252, "Editor": "robert.Snijders_Tronox"}},
                                 {"geometry": {"paths": [[pelican_dredge_xy[0], pelican_anchor_xy[1]]]}, "attributes": {"OBJECTID": 2, "Pelican_anchorLine_status": "active", "Shape__Length": 252.70348161905, "GlobalID": "b1319cd7-6a89-4be0-ace9-358e40be95e6", "CreationDate": 1552446220252, "Creator": "robert.Snijders_Tronox", "EditDate": 1552446220252, "Editor": "robert.Snijders_Tronox"}}]

        pelican_anchor_lyr = pelican_anchor_lines[0].layers[0]
        pelican_anchor_lyr.edit_features(updates = pelican_updateFeature)
    except:
        raise

def timeConvert(monthsToDelete):
    try:
        d = datetime.datetime(monthsToKeep.year, monthsToKeep.month, monthsToKeep.day, monthsToKeep.hour, monthsToKeep.minute, monthsToKeep.second)
        unixtime = time.mktime(d.timetuple())
        unixOutput = int(unixtime*1000)
        return unixOutput
    except:
        raise

def deleteFeatures():
    # selects features based on months defined and deletes them
    try:
        pelican_gpsQ = PELICANGPS_item[0].layers[0].query()
        pelican_features = pelican_gpsQ.features
        
        deleteFeatures = [f for f in pelican_features if f.attributes['Date_Stamp'] < timeConvert(monthsToKeep)]
        object_ids = [str(f.get_value('ObjectId')) for f in deleteFeatures]
        object_ids_str = ",".join(object_ids)
        pelican_gpsdata.edit_features(deletes = object_ids_str)
    except:
        raise

def init():
    connect()
    readCSV()
    featureEngineer()
    savePublish()
    updateStatus()
    appendingData()
    updateAnchorLine()
    deleteFeatures()

def main():
    init()

if __name__ == '__main__':
    main()

