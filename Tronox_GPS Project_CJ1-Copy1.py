
# coding: utf-8

# In[1]:


# This script reads gps data as csv and updates CJ1 dredge feature service
# and the anchor lines attached to it in real time.
# Author: Mehdi Pira - Esri Australia
# Created: 20190320
# Updated: 20190701

from arcgis.gis import GIS
from arcgis.features import SpatialDataFrame
from arcgis.features import FeatureLayer
import pandas as pd
import numpy as np
import time
import datetime
import config_cj1
import os
from arcgis import geometry
from copy import deepcopy

# define variables
gis = None
df = None
append_item = None
CJ1GPS_item = None
cj1_gpsdata = None
monthsToKeep = datetime.datetime.today() - datetime.timedelta(config_cj1.CONFIG['NoOfMonths']*365/12)

def connect():
    # connnect to ArcGIS Online
    try:
        global gis
        gis = GIS(config_cj1.CONFIG['url'], config_cj1.CONFIG['username'], config_cj1.CONFIG['password'], verify_cert=True)
    except:
        raise

def readCSV():
    # read the csv file and create column names and also get rid of empty rows
    try:
        global df
        inputCSV = os.path.join(config_cj1.CONFIG['dataWorkspace'], config_cj1.CONFIG['csvfile'])
        df = pd.read_csv(inputCSV, header=None)
        df.columns =['Dredge', 'Format', 'Time', 'Valid', 'Latitude', 'S_N', 'Longitude', 'E_W', 'Speed', 'True_Course', 'Date_Stamp', 'Variation', 'W_E', 'Check_Sum']
        df.dropna(axis=0, how='any', thresh=None, subset=None, inplace=True)
    except:
        raise
    

def featureEngineer():
    # convert latitudes/longitudes from DMD to DD, standardize date and time columns and flag latest date as active
    try:
        # Latitude conversion
        df['Latitude'] = df['Latitude'].astype(str)
        degree = df['Latitude'].str.split('.').str[0].str[:2].astype(float)
        minutes = (df['Latitude'].str.split('.').str[0].str[2:])
        decimalMinute = (minutes + '.' + df['Latitude'].str.split('.').str[1]).astype(float)/60
        df['Latitude'] = degree + decimalMinute
        df['Latitude'] = df['Latitude'] * -1

        # Longitude conversion
        df['Longitude'] = df['Longitude'].astype(str)
        degree2 = df['Longitude'].str.split('.').str[0].str[:3].astype(float)
        minutes2 = df['Longitude'].str.split('.').str[0].str[3:]
        decimalMinute2 = (minutes2 + '.' + df['Longitude'].str.split('.').str[1]).astype(float)/60
        df['Longitude'] = degree2 + decimalMinute2

        # Date and Time manipulation
        df['Time']=df['Time'].apply(lambda x: '{0:0>8}'.format(x))
        df['Time'] = df['Time'].apply(str)  
        df['Date_Stamp'] = df['Date_Stamp'].astype(float).astype(int)
        dtime = df['Date_Stamp'].apply(str) + ' ' + df['Time']
        df['Date_Stamp'] = pd.to_datetime(dtime, format='%d%m%y %H%M%S.%f', utc=True)

        # Flagging the latest recorded time of the gps location as active
        df['Loc_Status'] = np.where(df['Date_Stamp'] >= df['Date_Stamp'].max(), 'active', 'inactive')
    except:
        raise

def savePublish():
    # publish the csv into ArcGIS Online
    try:
        global append_item
        global outTempcsv
        outTempcsv = os.path.join(config_cj1.CONFIG['tempWorkspace'], config_cj1.CONFIG['tempCSVname'])
        df.to_csv(outTempcsv)        
    except:
        raise

def updateStatus():
    try:
        # Update status field
        global cj1_gpsdata
        global CJ1GPS_item
        CJ1GPS_item = gis.content.search('type: "Feature Service" AND title: "GPS CJ1"')
        cj1_gpsdata = CJ1GPS_item[0].layers[0]
        cj1_activeRow = cj1_gpsdata.query(where="Loc_Status = 'active'")
        if cj1_activeRow:
            cj1_activeRow.features[0].attributes['Loc_Status'] = 'inactive'
            cj1_gpsdata.edit_features(updates = cj1_activeRow.features)
        else:
            print('There is no active row!')
    except Exception as e:
        raise
        
def appendingData():
    # append the temp feature service which is the latest gps data captured to the existing CJ1 gps feature service
    try:
        cj1_fset = cj1_gpsdata.query()
        new_rows = pd.read_csv(outTempcsv)
        
        features_to_be_added = []

        # get a template feature object
        template_feature = deepcopy(cj1_fset.features[0])

        # loop through each row and add to the list of features to be added
        for row in new_rows.iterrows():
            new_feature = deepcopy(template_feature)

            #print
            print("Appending to " + row[1]['Dredge'])

            #get geometries in the destination coordinate system
            input_geometry = {'y':float(row[1]['Latitude']),
                               'x':float(row[1]['Longitude'])}
            output_geometry = geometry.project(geometries = [input_geometry],
                                               in_sr = 4326, 
                                               out_sr = cj1_fset.spatial_reference['latestWkid'],
                                               gis = gis)

            # assign the updated values
            new_feature.geometry = output_geometry[0]
            new_feature.attributes['Longitude'] = float(row[1]['Longitude'])
            new_feature.attributes['Dredge'] = row[1]['Dredge']
            new_feature.attributes['Format'] = row[1]['Format']
            new_feature.attributes['Time'] = float(row[1]['Time'])
            new_feature.attributes['Latitude'] = float(row[1]['Latitude'])
            new_feature.attributes['Valid'] = row[1]['Valid']
            new_feature.attributes['S_N'] = row[1]['S_N']
            new_feature.attributes['E_W'] = row[1]['E_W']
            new_feature.attributes['Speed'] = float(row[1]['Speed'])
            new_feature.attributes['True_Course'] = int(row[1]['True_Course'])
            new_feature.attributes['Date_Stamp'] = row[1]['Date_Stamp']
            new_feature.attributes['Variation'] = float(row[1]['Variation'])
            new_feature.attributes['W_E'] = row[1]['W_E']
            new_feature.attributes['Check_Sum'] = row[1]['Check_Sum']
            new_feature.attributes['Loc_Status'] = row[1]['Loc_Status']

            #add this to the list of features to be updated
            features_to_be_added.append(new_feature)
        
        cj1_gpsdata.edit_features(adds = features_to_be_added)
        
    except:
        raise

def updateAnchorLine():
    # update anchor lines by feeding the active dredge and anchor coordinates
    try:
        activePoint = cj1_gpsdata.query(where="Loc_Status = 'active'")
        cj1_dredge_x = [x for x in activePoint.df.Longitude.values]
        cj1_dredge_y = [y for y in activePoint.df.Latitude.values]
        cj1_dredge_xy = [list(xy) for xy in zip(cj1_dredge_x, cj1_dredge_y)]

        anchor_points = gis.content.search('type:"Feature Service" AND title: "Anchor points"')
        anchor_layer = anchor_points[0].layers[0]
        anchor_lyr = anchor_layer.query(where = "(Name = 'CJ1 port anchor' OR Name = 'CJ1 starboard anchor') OR Status = 'active'")
        anchor_x = [coords.geometry['x'] for coords in anchor_lyr]
        anchor_y = [coords.geometry['y'] for coords in anchor_lyr]
        anchor_xy = [list(xy) for xy in zip(anchor_x, anchor_y)]

        cj1_anchor_lines = gis.content.search('type:"Feature Service" AND title: "CJ1 Anchor Lines"')
        #cj1_anchor_layer = cj1_anchor_lines[0].layers[0].query()

        cj1_updateFeature = [{"geometry": {"paths": [[cj1_dredge_xy[0], anchor_xy[0]]]}, "attributes": {"OBJECTID": 6, "Line_Status": "In Use", "created_user": "robert.Snijders_Tronox", "created_date": 1552372199877, "last_edited_user": "robert.Snijders_Tronox", "last_edited_date": 1552444134330, "Shape__Length": 0.00301102339221567, "GlobalID": "9c0ccb45-6507-43a2-8b3e-f5ba456064ba"}},
                             {"geometry": {"paths": [[cj1_dredge_xy[0], anchor_xy[1]]]}, "attributes": {"OBJECTID": 8, "Line_Status": "In Use", "created_user": "robert.Snijders_Tronox", "created_date": 1552372199877, "last_edited_user": "robert.Snijders_Tronox", "last_edited_date": 1552444142680, "Shape__Length": 0.00318616894068179, "GlobalID": "7e6c7730-4710-4561-b94b-7e0db5467bd6"}}]

        cj1_anchor_lyr = cj1_anchor_lines[0].layers[0]
        cj1_anchor_lyr.edit_features(updates = cj1_updateFeature)
    except:
        raise

def timeConvert(monthsToDelete):
    try:
        d = datetime.datetime(monthsToKeep.year, monthsToKeep.month, monthsToKeep.day, monthsToKeep.hour, monthsToKeep.minute, monthsToKeep.second)
        unixtime = time.mktime(d.timetuple())
        unixOutput = int(unixtime*1000)
        return unixOutput
    except:
        raise
        
def deleteFeatures():
    # selects features based on months defined and deletes them
    try:
        cj1_gpsdataQ = CJ1GPS_item[0].layers[0].query()
        cj1_features = cj1_gpsdataQ.features

        deleteFeatures = [f for f in cj1_features if f.attributes['Date_Stamp'] < timeConvert(monthsToKeep)]
        object_ids = [str(f.get_value('ObjectId')) for f in deleteFeatures]
        object_ids_str = ",".join(object_ids)
        cj1_gpsdata.edit_features(deletes = object_ids_str)
    except:
        raise

def init():
    connect()
    readCSV()
    featureEngineer()
    savePublish()
    updateStatus()
    appendingData()
    updateAnchorLine()
    deleteFeatures()
    
def main():
    init()

if __name__ == '__main__':
    main()

